Pod::Spec.new do |s|
  s.name         = "PointCheckoutProgramSdk"
  s.version      = "1.0.24"
  s.summary      = "PointCheckout Program Sdk"
  s.description  = "PointCheckout Program Sdk used for payments"
  s.homepage     = "https://bitbucket.org/issuerhub/ios-pos-sdk"
  
  s.license      = { :type => 'PointCheckout', :file => 'LICENSE' }
  s.author       = { "PointCheckout" => "info@pointcheckout.com" }
  s.platform     = :ios, "10.0"
  s.source       = { :git => "https://bitbucket.org/issuerhub/ios-pos-sdk.git", :tag => "v#{s.version}" }
  s.source_files = "program-ios-sdk/**/*.{swift,h, m, rb}", "program-ios-sdk/*.{swift,h, m, rb}"
  s.resources      = "program-ios-sdk/Bundle/*.bundle", "assets/*", "program-ios-sdk/Font/*.{ttf}"
  s.preserve_paths = "program-ios-sdk/**/*"

  s.requires_arc = true
  s.swift_version= '5.0'
    
  s.dependency 'React'
  s.dependency 'React-Core'
  s.dependency 'ReactCommon'
  s.dependency 'React-CoreModules'
  s.dependency 'FBLazyVector'
  s.dependency 'FBReactNativeSpec'
  s.dependency 'RCTRequired'
  s.dependency 'RCTTypeSafety'

  s.dependency 'RNGestureHandler'
  s.dependency 'RNCMaskedView'
  s.dependency 'react-native-safe-area-context'
  s.dependency 'RNScreens', '~> 2.0.0-beta.2'

  s.dependency 'RNFastImage'
  s.dependency 'RNColorMatrixImageFilters'
  s.dependency 'react-native-webview'
  s.dependency 'RNSnackbar'
  s.dependency 'RNVectorIcons'
  s.dependency 'RNSVG'
  s.dependency 'react-native-camera'
  s.dependency 'ReactNativeLocalization'
  s.dependency 'react-native-slider'
  s.dependency 'RNLocalize'

end
