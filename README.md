# PointCheckout Program IOS SDK #

## Getting started ##

These instructions will help you to use the SDK within your IOS Swift application.

### Step 1 ###
Add PointCheckoutRepo to cocoapods by executing the following in the terminal:

`pod repo add PointCheckoutRepo git@bitbucket.org:pointcheckout-team/program-ios-sdk-pod-dependencies.git`

### Step 2 ###
Add the following on the top of your pod file:

```
source 'https://{user-id}@bitbucket.org/pointcheckout-team/program-ios-sdk-pod-dependencies.git'
source 'https://github.com/CocoaPods/Specs.git'
```

> NOTE: replace `{user-id}` with your bitbucket user id.

### Step 3 ###
Add `PointCheckoutProgramSdk` pod

```
pod 'PointCheckoutProgramSdk', :git => 'https://{user-id}@bitbucket.org/pointcheckout-team/program-ios-sdk.git', :tag => '{tagName}'
```
> NOTE: replace `{tagName}` with a tag from the tags sections, the latest version at the time writing this documentaion was `v1.0.10`
> NOTE: replace `{user-id}` with your bitbucket user id.

### Step 4 ###
Add the following to the bottom of your pod file:

```
post_install do |installer|
  require_relative 'Pods/PointCheckoutProgramSdk/program-ios-sdk/scripts/pointcheckout_post_install'
  pointcheckout_post_install()
end
```

After that execute `pod install`

### Step 5 ###

Create `RNEventHandler.m` with a bridging header:


```objectivec
//
//  RNEventHandler.m
//

#import <Foundation/Foundation.h>
#import <React/RCTEventEmitter.h>
#import <React/RCTBridge.h>

@interface RCT_EXTERN_MODULE(NativeEventHandler, RCTEventEmitter)
RCT_EXTERN_METHOD(onEvent:(NSString *) string)
@end

@interface RCT_EXTERN_MODULE(RNEventHandler, NSObject)
RCT_EXTERN_METHOD(onEvent:(NSString *) string)
@end
```

Create `RNEventHandler.swift`:

```swift
import Foundation
import PointCheckoutProgramSdk

@objc(RNEventHandler)
class RNEventHandler: NSObject {
    @objc func onEvent(_ value: String) -> Void {
        PointCheckoutRNHandler.onEvent(value);
    }
}
```

Create `NativeEventHandler.swift`:

```swift
import Foundation
import React
import PointCheckoutProgramSdk

@objc(NativeEventHandler)
class NativeEventHandler: PointCheckoutEventHandler {

    public static var instance:PointCheckoutEventHandler?

    override init() {
        super.init()
        NativeEventHandler.instance = self
    }

    override func supportedEvents() -> [String]! {
        return super.supportedEvents();
    }

    @objc override func onEvent(_ value: String) -> Void {
        super.onEvent(value);
    }
}
```
 

### Step 6 ###
The SDK uses some fonts to display icons, these fonts should be added to your info plist:

*  Create a directory called `Fonts` inside your project.
*  Go to `{YourProjectDirectory}/Pods/RNVectorIcons/Fonts`.
*  Copy `AntDesign, Ionicons, MaterialCommunityIcons, SimpleLineIcons` fonts to the directory you created inside your project.
*  Add the following to to your `Info.plist`:


```xml
    <key>UIAppFonts</key>
    <array>
        <string>AntDesign.ttf</string>
        <string>Ionicons.ttf</string>
        <string>MaterialCommunityIcons.ttf</string>
        <string>SimpleLineIcons.ttf</string>
    </array>
    
```

### Step 7 ###

Add camera permission to your `Info.plist`:
 
```xml
    <key>NSCameraUsageDescription</key>
    <string>Camera permission is needed to scan QR codes</string>
```

At this point, the SDK is integerated and ready to be used.

## Using the SDK ##

### Step 1: Initialization ###

The SDK needs to be initialized before showing a screen, its best to initialize the SDK after app startup. However, you can initialize the SDK right before showing a screen but the performance will be reduced.

To initialize the SDK you need a `config` object and a callback that returns an instance of the previously created `NativeEventHandler`. Bellow is a full example on how to initialize the SDK:

```swift
        // setup your theme here
        let theme = PointCheckoutTheme() 
        
        // create a config object
        let config = PointCheckoutConfig.Builder()
            .environment(PointCheckoutEnvironment.TEST) // set the env [TEST, PRODUCTION]
            .language(PointCheckoutLanguage.ENGLISH) // set the language [ARABIC, ENGLISH]
            .theme(theme) // set the theme
            .handleExceptions(true) // if true the SDK will show a message on errors
            .build();
            
        // initialize the SDK
        PointCheckoutClient.initialize(config, {() in
            // we pass the listener this way because
            // it is created after initialization by React Native
            return NativeEventHandler.instance
        })
        
        
        // event listener to be used for analytics
        PointCheckoutClient.onAnalyticsLog { (event, data) in
            print(event);
        }
        
        // this will be called whenever an exception is thrown
        // intended to be used for reporting exception
        PointCheckoutClient.onException { (value) in
            print(value)
        }
```

> NOTE: Initializing the SDK more than once has no effect

### Step 2: Show a screen ###

To show a screen, an `authToken` is needed, this token can be obtained from PointCheckout API.

```swift
        // shows the main screen
        PointCheckoutClient.presentMainController(self, "authToken");
        
        // shows the merchant list screen
        PointCheckoutClient.presentMerchantListController(self, "authToken");
        
        // shows a single merchant screen
        PointCheckoutClient.presentMerchantDetailsController(self, "authToken", "merchantId");
        
```

## Universal Links ##
If you dont already have Universal Links enabled, follow [this guid](https://developer.apple.com/ios/universal-links/).

### Step 1 ###
Send your app ID to PointCheckout, to add it to the apple-app-site-association file. Your app id should look like the follwoing: `{team-id}.{bundle-identifier}`

### Step 2 ###
Add PointCheckout URL to your Associated Domains:

| Environment        | URL                                                                                                       |
|--------------------|-----------------------------------------------------------------------------------------------------------|
| Test               | applinks:pay.test.pointcheckout.com                                                                       |
| Production         | applinks:pay.pointcheckout.com                                                                            |

### Step 3 ###
Send all PointCheckout URLs to the SDK:
```swift
 PointCheckoutClient.handleDeeplinkingUrl("The URL");
```

## PointCheckoutTheme ##
This class is used to customize the UI of the SDK. The table below shows the basic properties:

| Property        | Type                                  | Default      | Description              |
|-----------------|---------------------------------------|--------------|--------------------------|
| backgroundImage | String (image name from assets)       | nil          | Background image         |
| headerImage     | String (image name from assets)       | nil          | Main screen header image |
| headerVisible   | Bool                                  | true         | Hide or show the header  |
| statusBar       | ENUM("light-content", "dark-content") | dark-content | Status bar theme         |
| themeColor      | String                                | #ee8801      | Main theme color         |
| backgroundColor | String                                | #fafafa      | Background color         |
| foregroundColor | String                                | #4A4A4A      | Foreground color         |

## License ##

PointCheckout.com. All Rights Reserved.
