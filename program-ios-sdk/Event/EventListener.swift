//
//  EventListener.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 2/19/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation

class EventListener {
    
    public static let instance: EventListener = EventListener()
    
    private let manager:EventManager;
    
    private init() {
        self.manager = EventManager()
    }
    
    
    func addEventListener(event: PointCheckoutEvent, onEvent: @escaping (Any?) -> Void){
        self.manager.addListener(eventName: event.description, newEventListener: EventListenerAction(callback: onEvent))
    }
    
    func fireEvent(event: PointCheckoutEvent, data:Any? = nil){
        self.manager.trigger(eventName: event.description, information: data)
    }
    
    func fireEvent(event: String, data:Any? = nil){
        self.manager.trigger(eventName: event, information: data)
    }
}
