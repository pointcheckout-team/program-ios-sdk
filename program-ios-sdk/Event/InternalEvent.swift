//
//  InternalEvent.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 7/13/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation

public enum InternalEvent: String, CustomStringConvertible {
    case DEEPLINKING = "DEEPLINKING"
    case SDK_OPEN = "SDK_OPEN"

    public var description: String {
        get {
            return self.rawValue
        }
    }
}
