//
//  PointCheckoutClient.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 2/8/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import UIKit
import React

public class PointCheckoutClient {
    
    private static var rnController = UIViewController();
    private static var config : PointCheckoutConfig?;
    private static var getEventListener: (() -> PointCheckoutEventHandler?)?;
    private static var initialized: Bool = false;
    private static var onBackPressedListener: Bool = false;
    private static var onExceptionListener: Bool = false;
    private static var onAnalyticsLogListener: Bool = false;
    private static var showScreenEvent: RNEventWrapper?;
    private static var deeplinkEvent: RNEventWrapper?;

    public static func initialize(_ config: PointCheckoutConfig, _ getEventListener: @escaping () -> PointCheckoutEventHandler?){
        
        if(initialized){
            self.config?.language = config.language;
            self.config?.theme = config.theme;
            return;
        }

        self.rnController.view = RNLoader(config).getView();
        self.rnController.modalPresentationStyle = .overFullScreen
        self.config = config;
        self.getEventListener = getEventListener;
        
        self.onBackPressed({() in
            DispatchQueue.main.async {
                self.rnController.dismiss(animated: true, completion: nil)
            }
        });
        
        EventListener.instance.addEventListener(event: PointCheckoutEvent.INITIALIZED, onEvent: {(_ value: Any?) in
            if(self.showScreenEvent != nil){
                self.getEventListener!()?.onEvent(self.showScreenEvent!);
            }
            if(self.deeplinkEvent != nil){
                self.getEventListener!()?.onEvent(self.deeplinkEvent!);
            }
            self.initialized = true;
            self.showScreenEvent = nil;
        });
    }
    
    public static func getController() -> UIViewController {
        return self.rnController
    }
    
    public static func presentMainController(_ parent: UIViewController, _ authToken : String) {
        presentController(parent,authToken,PointCheckoutScreen.MAIN_SCREEN, nil);
    }
    
    public static func presentMerchantListController(_ parent: UIViewController, _ authToken : String) {
        presentController(parent,authToken,PointCheckoutScreen.MERCHANTS_SCREEN, nil);
    }
    
    public static func presentTransactionListController(_ parent: UIViewController, _ authToken : String) {
        presentController(parent,authToken,PointCheckoutScreen.TRANSACTIONS_SCREEN, nil);
    }
    
    public static func presentPromotionController(_ parent: UIViewController, _ authToken : String) {
        presentController(parent,authToken,PointCheckoutScreen.PROMOTIONS_SCREEN, nil);
    }
    
    public static func presentQuickPayController(_ parent: UIViewController, _ authToken : String) {
        presentController(parent,authToken,PointCheckoutScreen.QUICK_PAY_SCREEN, nil);
    }
    
    public static func presentMerchantDetailsController(_ parent: UIViewController, _ authToken : String, _ merchantId: String) {
        presentController(parent,authToken,PointCheckoutScreen.MERCHANT_DETAILS_SCREEN,"{\"id\":\"\(merchantId)\"}");
    }
    
    
    public static func presentController(_ parent: UIViewController, _ authToken : String, _ screen: PointCheckoutScreen, _ paramsJson: String?) {
        
        self.config?.authToken = authToken;
        self.config?.screen = screen ;
        self.config?.paramsJson = paramsJson;
        
        let wrapper = RNEventWrapper();
        wrapper.event = InternalEvent.SDK_OPEN.description;
        wrapper.data = self.config?.serialize();
        
        if(initialized) {
            self.getEventListener!()?.onEvent(wrapper);
        }else{
            self.showScreenEvent = wrapper;
        }
        
        if(UIApplication.topViewController() != rnController){
            parent.present(self.rnController, animated: true, completion: nil);
        }
       
    }
    
    
    private static func onBackPressed(_ callback: @escaping () -> Void){
        if(self.onBackPressedListener){
            return;
        }
        self.onBackPressedListener = true;
        EventListener.instance.addEventListener(event: PointCheckoutEvent.EXIT_REQUEST, onEvent: {(_ value: Any?) in
            callback();
        });
    }
    
    public static func onException(_ callback: @escaping (String) -> Void){
        if(self.onExceptionListener){
            return;
        }
        self.onExceptionListener = true;
        EventListener.instance.addEventListener(event: PointCheckoutEvent.EXCEPTION, onEvent: {(_ value: Any?) in
            callback(value as? String ?? "Unknown");
        });
    }
    
    public static func onAnalyticsLog(_ callback: @escaping (_ event: String, _ payload: Dictionary<String, Any>?) -> Void){
        if(self.onAnalyticsLogListener){
            return;
        }
        self.onAnalyticsLogListener = true;
        EventListener.instance.addEventListener(event: PointCheckoutEvent.LOG_ANALYTICS, onEvent: {(_ value: Any) in
            
            let data = JsonUtils.convertToDictionary(value as! String)
            callback(data?["event"] as! String, data?["payload"] as? Dictionary<String, Any>);
        });
    }
    
    public static func handleDeeplinkingUrl(_ url: String){
        let wrapper = RNEventWrapper();
        wrapper.event = InternalEvent.DEEPLINKING.description;
        wrapper.data = url;
        if(initialized) {
            self.getEventListener!()?.onEvent(wrapper);
        }else{
            self.deeplinkEvent = wrapper;
        }
    }
    
    public static func getDeviceId()-> String{
        return IdUtils.getDeviceId();
    }
    
}
