//
//  PointCheckoutTheme.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 2/27/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation

public class PointCheckoutTheme: Codable {
    
    public init(){}
    
    public var font: String?
    public var fontSize: Int?

    public var transactionsCard: Bool?

    public var backgroundImage: String?
    public var mainScreenLayout: Int?
    public var headerBackgroundImage: String?
    public var headerImage: String?

    public var headerVisible: Bool?
    public var nativeHeader: Bool?
    public var statusBar: String?
    
    public var themeColor: String?
    
    public var backgroundColor: String?
    public var foregroundColor: String?
    
    public var cardBorderColor: String?
    public var cardBackgroundColor: String?
    
    public var listItemBorderColor: String?
    public var listItemBorderRadius: Int?
    public var listItemBackgroundColor: String?
    public var listItemSecondaryColor: String?
    
    
    
    public var buttonBackgroundColor: String?
    public var buttonBorderColor: String?
    public var buttonForegroundColor: String?
    
    public var buttonSecondaryBackgroundColor: String?
    public var buttonSecondaryBorderColor: String?
    public var buttonSecondaryForegroundColor: String?
    
    public var buttonDisabledBackgroundColor: String?
    public var buttonDisabledBorderColor: String?
    public var buttonDisabledForegroundColor: String?
    
    public var toggleOnBackgroundColor: String?
    public var toggleOnCircleColor: String?
    
    public var toggleOffBackgroundColor: String?
    public var toggleOffCircleColor: String?
    
    public var snackbarBackgroundColor: String?
    public var snackbarForegroundColor: String?
    
    public var sliderTrackMinColor: String?
    public var sliderTrackMaxColor: String?
    public var sliderThumbColor: String?
    
    public var badgeBackgroundColor: String?
    public var badgeForegroundColor: String?
    
    public var iconColor: String?
    
    public var inputBackgroundColor: String?
    public var inputBorderColor: String?
    public var inputTextColor: String?
    public var inputHintColor: String?
    
    public var modalBackgroundColor: String?
    public var modalForegroundColor: String?
    public var modalInputBackgroundColor: String?
    public var modalInputBorderColor: String?
    public var modalInputTextColor: String?
    public var modalInputHintColor: String?
    
    
    public var errorColor: String?
    public var infoColor: String?
    public var warningColor: String?
    public var successColor: String?
    public var dangerColor: String?
    
    
    
    public func serialize(_ ignoreImages : Bool) -> String {
        let originalBackgroundImage = self.backgroundImage ?? nil;
        let originalHeaderImage = self.headerImage ?? nil;
        let originalHeaderBackgroundImage = self.headerBackgroundImage ?? nil;

        if(!ignoreImages && self.backgroundImage != nil){
            do {
                self.backgroundImage = try ImageUtils.assetToBase64(self.backgroundImage!);
            }catch{
                print("WARNING: failed to load asset with name \(String(describing: self.backgroundImage))")
                // ignore
            }
        }
        
        if(!ignoreImages && self.headerImage != nil){
            do {
                self.headerImage = try ImageUtils.assetToBase64(self.headerImage!);
            }catch{
                print("WARNING: failed to load asset with name \(String(describing: self.headerImage))")
                // ignore
            }
        }
        
        if(!ignoreImages && self.headerBackgroundImage != nil){
            do {
                self.headerBackgroundImage = try ImageUtils.assetToBase64(self.headerBackgroundImage!);
            }catch{
                print("WARNING: failed to load asset with name \(String(describing: self.headerBackgroundImage))")
                // ignore
            }
        }
        
        let json = JsonUtils.encode(self);
        
        self.backgroundImage = originalBackgroundImage;
        self.headerImage = originalHeaderImage;
        self.headerBackgroundImage = originalHeaderBackgroundImage;

        return json ?? "{}";
    }
    
}
