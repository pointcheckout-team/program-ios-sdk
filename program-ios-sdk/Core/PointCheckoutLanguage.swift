//
//  PointCheckoutLanguage.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 8/11/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation

public enum PointCheckoutLanguage: String, CustomStringConvertible {
    case ENGLISH = "en"
    case ARABIC = "ar"

    public var description: String {
        get {
            return self.rawValue
        }
    }
}
