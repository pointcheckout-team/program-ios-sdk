//
//  Environment.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 2/18/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//


import Foundation

public enum PointCheckoutEnvironment: String, CustomStringConvertible {
    case PRODUCTION = "PRODUCTION"
    case TEST = "TEST"
    case DEBUG = "DEBUG"
    
    public var description: String {
        get {
            return self.rawValue
        }
    }
}
