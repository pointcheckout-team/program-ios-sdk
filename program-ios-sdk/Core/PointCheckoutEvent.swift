//
//  Event.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 2/19/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation

public enum PointCheckoutEvent: String, CustomStringConvertible {
    case EXIT_REQUEST = "EXIT_REQUEST"
    case EXCEPTION = "EXCEPTION"
    case LOG_ANALYTICS = "LOG_ANALYTICS"
    case INITIALIZED = "INITIALIZED"

    public var description: String {
        get {
            return self.rawValue
        }
    }
}
