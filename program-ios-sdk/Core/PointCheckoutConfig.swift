//
//  PointCheckoutConfig.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 9/28/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation

public class PointCheckoutConfig {
    
    public var environment: PointCheckoutEnvironment?;
    public var screen: PointCheckoutScreen?;
    public var paramsJson: String?;
    public var authToken: String?;
    public var theme: PointCheckoutTheme?;
    public var language: PointCheckoutLanguage?;
    public var handleExceptions: Bool?;
    public var deeplinkingUrl: String?;
    
    public init() { }
    
    public class Builder {
        
        private var environment: PointCheckoutEnvironment?;
        private var screen: PointCheckoutScreen?;
        private var paramsJson: String?;
        private var authToken: String?;
        private var theme: PointCheckoutTheme?;
        private var language: PointCheckoutLanguage?;
        private var handleExceptions: Bool?;
        private var deeplinkingUrl: String?;
        
        public init() { }
        
        public func environment(_ environment: PointCheckoutEnvironment) -> Builder{
            self.environment = environment;
            return self;
        }
        
        public func screen(_ screen: PointCheckoutScreen) -> Builder{
            self.screen = screen;
            return self;
        }
        public func screenParams(_ paramsJson: String) -> Builder{
            self.paramsJson = paramsJson;
            return self;
        }
        
        
        public func authToken(_ authToken: String)-> Builder{
            self.authToken = authToken;
            return self;
        }
        
        public func theme(_ theme: PointCheckoutTheme)-> Builder{
            self.theme = theme;
            return self;
        }
        
        public func language(_ language: PointCheckoutLanguage)-> Builder{
            self.language = language;
            return self;
        }
        
        public func handleExceptions(_ handleExceptions: Bool)-> Builder{
            self.handleExceptions = handleExceptions;
            return self;
        }
        
        public func deeplinkingUrl(_ deeplinkingUrl: String)-> Builder{
            self.deeplinkingUrl = deeplinkingUrl;
            return self;
        }
        
        public func build() -> PointCheckoutConfig{
            let instance = PointCheckoutConfig();
            instance.environment = self.environment;
            instance.screen = self.screen;
            instance.paramsJson = self.paramsJson;
            instance.environment = self.environment;
            instance.authToken = self.authToken;
            instance.theme = self.theme;
            instance.language = self.language;
            instance.handleExceptions = self.handleExceptions;
            instance.deeplinkingUrl = self.deeplinkingUrl;
            return instance;
        }
    }
    
    private let PROPERTY_ENV: String = "ENV"
    private let PROPERTY_SCREEN: String = "OPEN_SCREEN"
    private let PROPERTY_SCREEN_PROPS: String = "SCREEN_PARAMS"
    private let PROPERTY_AUTH_TOKEN: String = "AUTH_TOKEN"
    private let PROPERTY_THEME: String = "THEME"
    private let PROPERTY_LANGUAGE: String = "LANGUAGE"
    private let PROPERTY_HANDLE_EXCEPTIONS: String = "HANDLE_EXCEPTIONS"
    private let DEEPLINK: String = "DEEPLINK"
    private let DEVICE_ID: String = "DEVICE_ID"
    
    public func toDictionary(_ ignoreImages:Bool) -> [String:String]{
        let theme: String = (self.theme ?? PointCheckoutTheme()).serialize(ignoreImages);
        
        return [
            PROPERTY_ENV:self.environment!.description,
            PROPERTY_SCREEN:self.screen?.description ?? "",
            PROPERTY_SCREEN_PROPS:self.paramsJson ?? "",
            PROPERTY_AUTH_TOKEN:self.authToken ?? "",
            PROPERTY_THEME:theme,
            PROPERTY_LANGUAGE: self.language?.description ?? PointCheckoutLanguage.ENGLISH.description,
            PROPERTY_HANDLE_EXCEPTIONS: self.handleExceptions?.description ?? false.description,
            DEEPLINK: self.deeplinkingUrl ?? "",
            DEVICE_ID: IdUtils.getDeviceId()
        ];
    }
    
    
    public func serialize() -> String{
        
        if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: self.toDictionary(false),
            options: .prettyPrinted
        ),
        let theJSONText = String(data: theJSONData,
                                 encoding: String.Encoding.ascii) {
            return theJSONText;
        }
        return "";
    }
    
}
