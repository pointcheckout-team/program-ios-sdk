//
//  PointCheckoutRNHandler.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 9/7/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation
import React

public class PointCheckoutRNHandler: NSObject {
    public static func onEvent(_ value: String) -> Void {
        let event = JsonUtils.decode(value, RNEventWrapper.self) ?? RNEventWrapper(value)
        EventListener.instance.fireEvent(event: event.event ?? value, data: event.data)
    }
}
