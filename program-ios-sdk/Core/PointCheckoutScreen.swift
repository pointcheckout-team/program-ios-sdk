//
//  PointCheckoutScreen.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 10/6/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation

public enum PointCheckoutScreen: String, CustomStringConvertible {
    case MAIN_SCREEN = "MainScreen"
    case MERCHANTS_SCREEN = "MerchantScreen"
    case MERCHANT_DETAILS_SCREEN = "MerchantDetailsScreen"
    case TRANSACTIONS_SCREEN = "TransactionScreen"
    case PROMOTIONS_SCREEN = "PromotionScreen"
    case QUICK_PAY_SCREEN = "QuickPayScreen"
    
    public var description: String {
        get {
            return self.rawValue
        }
    }
}
