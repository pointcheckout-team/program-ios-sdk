//
//  PointCheckoutEventHandler.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 9/7/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation
import React

open class PointCheckoutEventHandler: RCTEventEmitter {
    
    open override func supportedEvents() -> [String]! {
        return ["onEvent"]
    }
    
    open func onEvent(_ value: String) -> Void {
        self.sendEvent(withName: "onEvent", body: value)
    }
    
    public func onEvent(_ value: RNEventWrapper) -> Void {
        self.onEvent(JsonUtils.encode(value)!)
    }
}
