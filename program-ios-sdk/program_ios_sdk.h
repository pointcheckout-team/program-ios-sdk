//
//  program_ios_sdk.h
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 2/8/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for program_ios_sdk.
FOUNDATION_EXPORT double program_ios_sdkVersionNumber;

//! Project version string for program_ios_sdk.
FOUNDATION_EXPORT const unsigned char program_ios_sdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <program_ios_sdk/PublicHeader.h>


