//
//  RNAnalyticsEvent.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 9/16/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation


public class RNAnalyticsEvent: Codable {
    
    public init(){}
    public init(_ event: String, _ payload: Dictionary<String, String>){
        self.event = event
        self.payload = payload;
    }
    
    public var event: String?
    public var payload: Dictionary<String, String>?
}

