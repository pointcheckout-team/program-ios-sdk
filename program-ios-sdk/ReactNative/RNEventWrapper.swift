//
//  RNEventWrapper.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 7/13/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation


import Foundation

public class RNEventWrapper: Codable {
    
    public init(){}
    public init(_ event: String){
        self.event = event
    }
    
    public var event: String?
    public var data: String?
}

