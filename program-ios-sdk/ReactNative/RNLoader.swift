//
//  RNLoader.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 2/8/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import React

class RNLoader {
    
    private let MODULE_NAME: String = "programsdk"
    
    private let config: PointCheckoutConfig;
    
    public init(_ config: PointCheckoutConfig) {
        self.config = config;
    }

    public func getView() -> UIView{
        
        let JS_BUNDLE = self.getBundle()
            
        let view = RCTRootView(
            bundleURL: JS_BUNDLE,
            moduleName: MODULE_NAME,
            initialProperties: config.toDictionary(true),
            launchOptions: nil)
                
        return view;
        
    }
    
    private func getBundle()-> URL {
        if self.config.environment == PointCheckoutEnvironment.DEBUG {
            return URL(string: "http://localhost:8081/index.bundle?platform=ios")!
        }
        
        return Bundle(for: RNLoader.self).url(forResource: "index.ios", withExtension: "bundle")!
    }

}
