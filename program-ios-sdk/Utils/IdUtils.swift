//
//  IdUtils.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 9/22/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation

class IdUtils {
    
    private static let KEY_DEVICE_ID: String = "POINTCHECKOUT_PROGRAMSDK_DEVICE_ID"
    
    
    public static func getDeviceId()-> String{
        var storedId: String? = UserDefaults.standard.object(forKey: KEY_DEVICE_ID) as? String;
        
        if (storedId ?? "").isEmpty {
            storedId = UUID().uuidString;
            setDeviceId(storedId!);
        }
        return storedId!;
    }
    
    private static func setDeviceId(_ value: String){
        UserDefaults.standard.set(value, forKey: KEY_DEVICE_ID);
        UserDefaults.standard.synchronize();
    }
}
