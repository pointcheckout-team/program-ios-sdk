//
//  ImageUtils.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 8/12/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation
import UIKit

class ImageUtils {
    
    public static func assetToBase64(_ name: String) throws -> String {
        
        let image : UIImage = UIImage(named:name)!
        let imageData:Data = image.pngData()!
        
        return imageData.base64EncodedString(options: .lineLength64Characters)
    }
    
}
