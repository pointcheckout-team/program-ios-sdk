//
//  JsonUtils.swift
//  program-ios-sdk
//
//  Created by Abdullah Asendar on 7/13/20.
//  Copyright © 2020 PointCheckout. All rights reserved.
//

import Foundation

class JsonUtils {
    
    private static let jsonEncoder = JSONEncoder()
    private static let jsonDecoder = JSONDecoder()
    
    public static func encode<T : Codable>(_ value: T)-> String? {
        
        do{
            let jsonData = try jsonEncoder.encode(value)
            return String(data: jsonData, encoding: String.Encoding.utf8)
        }catch{
            print("Error encoding JSON: \(error)")
        }
        
        return nil;
    }
    
    public static func decode<T: Decodable>(_ value: String, _ type: T.Type)-> T?{

        do{
            return try jsonDecoder.decode(type, from: value.data(using: .utf8)!)
        }catch{
            print("Error decoding JSON: \(error)")
        }
        
        return nil;
    }
    
    public static func convertToDictionary(_ value: String) -> [String: Any]? {
        if let data = value.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
